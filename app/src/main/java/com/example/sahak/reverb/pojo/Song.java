package com.example.sahak.reverb.pojo;

/*
 * This is demo code to accompany the Mobiletuts+ series:
 * Android SDK: Creating a Music Player
 * 
 * Sue Smith - February 2014
 */

import android.os.Parcel;
import android.os.Parcelable;

public class Song implements Parcelable{

	private String title;
	private String artist;
    private long id,albumID,duration;

    public Song(String title, String artist,long id, long albumID,long duration) {

        this.title = title;
        this.artist = artist;
        this.id=id;
        this.albumID=albumID;
        this.duration=duration;
    }

    public long getAlbumID() {
        return albumID;
    }

    public long getDuration() {
        return duration;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setAlbumID(long albumID) {
        this.albumID = albumID;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public String getArtist() {
        return artist;
    }

   public long getId(){return id;}

    public Song() {
	}



	public void setTitle(String title) {
		this.title = title;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

    @Override
    public int describeContents() {
        return 0;
    }

    @SuppressWarnings("unused")
    public Song(Parcel in) {
        this();
        readFromParcel(in);
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {


        dest.writeString(title);
        dest.writeString(artist);
        dest.writeLong(id);
        dest.writeLong(albumID);
        dest.writeLong(duration);


    }
    private void readFromParcel(Parcel in) {

        this.title = in.readString();
        this.artist = in.readString();
        this.id = in.readLong();
        this.albumID = in.readLong();
        this.duration= in.readLong();
    }

    public static final Creator<Song> CREATOR = new Creator<Song>() {
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }

        public Song[] newArray(int size) {
            return new Song[size];
        }
    };

}
