package com.example.sahak.reverb.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sahak.reverb.R;
import com.example.sahak.reverb.pojo.Song;

import java.util.ArrayList;

/**
 * Created by Kingshuk on 25-Mar-16.
 */
public class SongAdapterRV extends RecyclerView.Adapter<SongAdapterRV.SongViewHolder> {
    private ArrayList<Song> songListP;

    FragmentManager manager;
    public static class SongViewHolder extends RecyclerView.ViewHolder {

        public TextView title, artist, duration;
        Context mContext;
        public SongViewHolder(View view) {
            super(view);
            mContext= view.getContext();
            title = (TextView) view.findViewById(R.id.song_title);
            artist = (TextView) view.findViewById(R.id.song_artist);
            duration = (TextView) view.findViewById(R.id.song_duration);


        }
    }


    public SongAdapterRV(ArrayList<Song> songListP) {
        this.songListP = songListP;

    }

    @Override
    public SongViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_song, parent, false);

        return new SongViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SongViewHolder holder, int position) {
        final Song song = songListP.get(position);
        holder.title.setText(song.getTitle());
        holder.artist.setText(song.getArtist());
        holder.duration.setText(convertSecondsToHMmSs(song.getDuration()/1000));
        Typeface tf = Typeface.createFromAsset(holder.title.getContext().getAssets(), "fonts/Quicksand-Bold.ttf");
        Typeface tf2 = Typeface.createFromAsset(holder.title.getContext().getAssets(), "fonts/Quicksand-Medium.ttf");
        Typeface tf3 = Typeface.createFromAsset(holder.title.getContext().getAssets(), "fonts/Economica-Regular.ttf");
        holder.title.setTypeface(tf2);
        holder.artist.setTypeface(tf2);
        holder.duration.setTypeface(tf3);

    }


    public static String convertSecondsToHMmSs(long seconds) {
        long s = seconds % 60;
        long m = (seconds / 60) % 60;
        long h = (seconds / (60 * 60)) % 24;
        return String.format("%d:%02d", m,s);
    }
    @Override
    public int getItemCount() {
        return songListP.size();
    }
}

