package com.example.sahak.reverb.fragment;

import android.app.Fragment;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.example.sahak.reverb.MainActivity;
import com.example.sahak.reverb.R;
import com.example.sahak.reverb.adapter.SongAdapterRV;
import com.example.sahak.reverb.pojo.Song;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


/**
 * Created by yarolegovich on 25.03.2017.
 */

public class SongListFragment extends Fragment {

    private static final String EXTRA_TEXT = "text";
    RecyclerView songRecyclerView;
    SongAdapterRV mAdapter;
    ArrayList<Song> songList = new ArrayList<>();


    public static SongListFragment createFor(String text) {
        SongListFragment fragment = new SongListFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_TEXT, text);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_song_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        String text = getArguments().getString(EXTRA_TEXT);
        TextView textView = (TextView) view.findViewById(R.id.text);
        textView.setText(text);

        final Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        if(toolbar!= null)
            toolbar.setElevation(0);

        //Set up Recyclerview
        songRecyclerView =(RecyclerView) view.findViewById(R.id.songList);


        prepareEntryData();

        mAdapter = new SongAdapterRV(songList);

        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(view.getContext());

        songRecyclerView.setLayoutManager(linearLayoutManager);
        songRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();


        final int initialTopPosition = songRecyclerView.getTop();

        // Set a listener to scroll view
        songRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(toolbar!= null && songRecyclerView.getChildAt(0).getTop() < initialTopPosition ) {
                    toolbar.setElevation(12);
                } else {
                    toolbar.setElevation(0);
                }
            }
        });
    }

    @Override
    public void onDetach() {

        super.onDetach();
        songRecyclerView.clearOnScrollListeners();
    }

    void prepareEntryData()
    {
        ContentResolver musicResolver = getActivity().getContentResolver();
        Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);
        //iterate over results if valid
        if (musicCursor != null && musicCursor.moveToFirst()) {
            //get columns
            int titleColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.TITLE);
            int idColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media._ID);
            int artistColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.ARTIST);
            int duration = musicCursor.getColumnIndex(MediaStore.Audio.Media.DURATION);
            int albumIDcolumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);
            //add songs to list


            do {

                long thisId = musicCursor.getLong(idColumn);
                long albumID = musicCursor.getLong(albumIDcolumn);
                String thisTitle = musicCursor.getString(titleColumn);
                String thisArtist = musicCursor.getString(artistColumn);
                long d = musicCursor.getLong(duration);

                if (d > 30000) {
                        songList.add(new Song(thisTitle, thisArtist, thisId, albumID,d));

                    }
            } while (musicCursor.moveToNext());


            if (songList.size() > 0) {
                Collections.sort(songList, new Comparator<Song>() {
                    @Override
                    public int compare(final Song object1, final Song object2) {
                        return object1.getTitle().compareTo(object2.getTitle());
                    }
                });
            }
        }
    }


}

