package com.example.sahak.reverb;

import android.app.Fragment;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.example.sahak.reverb.fragment.SongListFragment;
import com.example.sahak.reverb.fragment.StatisticsFragment;
import com.example.sahak.reverb.menu.DrawerAdapter;
import com.example.sahak.reverb.menu.DrawerItem;
import com.example.sahak.reverb.menu.SimpleItem;
import com.example.sahak.reverb.menu.SpaceItem;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;


import java.util.Arrays;

/**
 * Created by yarolegovich on 25.03.2017.
 */

public class MainActivity extends AppCompatActivity implements DrawerAdapter.OnItemSelectedListener {

    private static final int POS_DASHBOARD = 0;
    private static final int POS_ACCOUNT = 1;
    private static final int POS_MESSAGES = 2;
    private static final int POS_CART = 3;
    private static final int POS_LOGOUT = 5;

    private String[] screenTitles;
    private Drawable[] screenIcons;
    Toolbar toolbar;

    SlidingRootNav mSlideMenu;
    TextView title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        screenIcons = loadScreenIcons();
        screenTitles = loadScreenTitles();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        title=(TextView)findViewById(R.id.title);
        title.setText("My Music");

        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/Quicksand-Bold.ttf");
        title.setTypeface(tf);

        toolbar.post(new Runnable() {
            @Override
            public void run() {
                Drawable d = ResourcesCompat.getDrawable(getResources(), R.drawable.menu_icon, null);
                toolbar.setNavigationIcon(d);
            }
        });

        initSlideMenu();


        DrawerAdapter adapter = new DrawerAdapter(Arrays.asList(
                createItemFor(POS_DASHBOARD).setChecked(true),
                createItemFor(POS_ACCOUNT),
                createItemFor(POS_MESSAGES),
                createItemFor(POS_CART),
                new SpaceItem(48),
                createItemFor(POS_LOGOUT)));
        adapter.setListener(this);

        RecyclerView list = (RecyclerView) findViewById(R.id.list);
        list.setNestedScrollingEnabled(false);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(adapter);

        adapter.setSelected(POS_DASHBOARD);
    }

    private void initSlideMenu() {
        mSlideMenu = new SlidingRootNavBuilder(this)
                .withMenuLayout(R.layout.menu_left_drawer)
                .withMenuOpened(false)
                .withToolbarMenuToggle(toolbar)
                .inject();

        TextView title= (TextView)mSlideMenu.getLayout().findViewById(R.id.title);
        Typeface tf2 = Typeface.createFromAsset(this.getAssets(), "fonts/PatrickHand-Regular.ttf");
        title.setTypeface(tf2);
    }



    @Override
    public void onItemSelected(int position) {



            switch (position) {

                case 0:
                    title.setText(screenTitles[position]);
                    Fragment selectedScreen = SongListFragment.createFor(screenTitles[position]);
                    showFragment(selectedScreen);

                    break;
                case 1:
                    title.setText(screenTitles[position]);
                    selectedScreen = StatisticsFragment.createFor(screenTitles[position]);
                    showFragment(selectedScreen);

                    break;
                case 2:
                    title.setText(screenTitles[position]);
                    selectedScreen = StatisticsFragment.createFor(screenTitles[position]);
                    showFragment(selectedScreen);
                    break;
                case 3:
                    title.setText(screenTitles[position]);
                    selectedScreen = StatisticsFragment.createFor(screenTitles[position]);
                    showFragment(selectedScreen);
                    break;
                case 5:
                    finish();

            }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mSlideMenu.closeMenu();
            }
        }, 135);
        //

    }


    private void showFragment(Fragment fragment) {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    private DrawerItem createItemFor(int position) {
        return new SimpleItem(screenIcons[position], screenTitles[position])
                .withIconTint(color(R.color.textColorSecondary))
                .withTextTint(color(R.color.textColorSecondary))
                .withSelectedIconTint(color(R.color.colorAccent))
                .withSelectedTextTint(color(R.color.colorAccent));
    }

    private String[] loadScreenTitles() {
        return getResources().getStringArray(R.array.ld_activityScreenTitles);
    }

    private Drawable[] loadScreenIcons() {
        TypedArray ta = getResources().obtainTypedArray(R.array.ld_activityScreenIcons);
        Drawable[] icons = new Drawable[ta.length()];
        for (int i = 0; i < ta.length(); i++) {
            int id = ta.getResourceId(i, 0);
            if (id != 0) {
                icons[i] = ContextCompat.getDrawable(this, id);
            }
        }
        ta.recycle();
        return icons;
    }

    @ColorInt
    private int color(@ColorRes int res) {
        return ContextCompat.getColor(this, res);
    }
}
